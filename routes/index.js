
/*
 * GET home page.
 */

var clients = require('../data');

var client = require('../client');

for(var id in clients) {
  clients[id] = client(clients[id]);
}


exports.client = function(req, res){
  var id = req.param('id');

  if(typeof clients[id] === 'undefined') {
    res.status(404).json({status: 'error'});
  }
  else{
    res.json(clients[id].getInformation());
  }
};