var Client = function() {
  this.data = {
    id      : null,
    name    : null,
    join    : null,
    expiry  : null
  };

  this.fill = function(info) {
    for(var prop in this.data){
      if(this.data[prop] !== 'undefined') {
        this.data[prop] = info[prop];
      }
    } 
  };

  this.getInformation = function() {
    return this.data;
  };
};

module.exports = function (info) {
  var instance = new Client();
  instance.fill(info);
  return instance;
};